if (typeof process.env.NO_DB === 'undefined') {
  process.env.NO_DB = 'yes'
}
const app = require('../src/app')

// eslint-disable-next-line no-console
console.log(JSON.stringify(app.locals.api.apiDoc, null, 2))
