module.exports = user

function user(articleService) {
  const operations = {
    POST
  }

  async function POST(req, res, next) {
    const id = await articleService.update(
      req.app.locals.db,
      req.body.articleId,
      req.body.userId,
      req.body.title,
      req.body.text,
      req.body.tags
    )
    res.json(id)
  }

  POST.apiDoc = {
    summary: 'Update an article',
    operationId: 'updateArticle',
    tags: [
      'article'
    ],
    security: [
      { keyScheme: [] }
    ],
    requestBody: {
      content: {
        'application/json': {
          schema: {
            type: 'object',
            properties: {
              articleId: {
                type: 'string',
              },
              userId: {
                type: 'string',
              },
              title: {
                type: 'string',
              },
              text: {
                type: 'string',
              },
              tags: {
                type: 'array',
                items: {
                  type: 'string',
                  minLength: 1,
                }
              }
            },
            required: [
              'articleId',
              'userId',
              'title',
              'text',
              'tags',
            ],
          },
        },
      },
      required: true,
    },
    responses: {
      200: {
        description: 'Article ID',
        content: {
          'application/json': {
            schema: {
              type: 'string',
            }
          }
        }
      },
    }
  }

  return operations
}
