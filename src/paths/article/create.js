module.exports = user

function user(articleService) {
  const operations = {
    POST
  }

  async function POST(req, res, next) {
    const id = await articleService.create(
      req.app.locals.db,
      req.body.userId,
      req.body.title,
      req.body.text,
      req.body.tags
    )
    res.json(id)
  }

  POST.apiDoc = {
    summary: 'Create an article',
    operationId: 'createArticle',
    tags: [
      'article'
    ],
    security: [
      { keyScheme: [] }
    ],
    requestBody: {
      content: {
        'application/json': {
          schema: {
            type: 'object',
            properties: {
              userId: {
                type: 'string',
              },
              title: {
                type: 'string',
              },
              text: {
                type: 'string',
              },
              tags: {
                type: 'array',
                items: {
                  type: 'string',
                  minLength: 1,
                }
              }
            },
            required: [
              'userId',
              'title',
              'text',
              'tags',
            ],
          },
        },
      },
      required: true,
    },
    responses: {
      200: {
        description: 'Article ID',
        content: {
          'application/json': {
            schema: {
              type: 'string',
            }
          }
        }
      },
    }
  }

  return operations
}
