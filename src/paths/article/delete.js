module.exports = user

function user(articleService) {
  const operations = {
    POST
  }

  async function POST(req, res, next) {
    const id = await articleService.dellete(
      req.app.locals.db,
      req.body.articleId,
    )
    res.json(id)
  }

  POST.apiDoc = {
    summary: 'Delete an article',
    operationId: 'deleteArticle',
    tags: [
      'article'
    ],
    security: [
      { keyScheme: [] }
    ],
    requestBody: {
      content: {
        'application/json': {
          schema: {
            type: 'object',
            properties: {
              articleId: {
                type: 'string',
              },
            },
            required: [
              'articleId',
            ],
          },
        },
      },
      required: true,
    },
    responses: {
      200: {
        description: 'Article ID',
        content: {
          'application/json': {
            schema: {
              type: 'string',
            }
          }
        }
      },
    }
  }

  return operations
}
