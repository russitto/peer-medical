module.exports = user

function user(articleService) {
  const operations = {
    GET
  }

  async function GET(req, res, next) {
    const articles = await articleService.find(
      req.app.locals.db,
      req.query.tag,
    )
    res.json(articles)
  }

  GET.apiDoc = {
    summary: 'Create an article',
    operationId: 'createArticle',
    tags: [
      'article'
    ],
    security: [
      { keyScheme: [] }
    ],
    parameters: [
      {
        name: 'tag',
        in: 'query',
        required: true,
        schema: {
          type: 'string',
        }
      }
    ],
    responses: {
      200: {
        description: 'Article Set',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: {
                type: 'object',
                properties: {
                  _id: {
                    type: 'string',
                  },
                  userId: {
                    type: 'string',
                  },
                  title: {
                    type: 'string',
                  },
                  text: {
                    type: 'string',
                  },
                  tags: {
                    type: 'array',
                    items: {
                      type: 'string',
                    },
                  }
                }
              }
            }
          }
        }
      },
    }
  }

  return operations
}
