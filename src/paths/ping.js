module.exports = ping

function ping(helloService) {
  const operations = {
    GET
  }

  function GET(req, res, next) {
    res.json(helloService.ping())
  }

  GET.apiDoc = {
    summary: 'Returns pong',
    operationId: 'getPing',
    tags: [
      'hello'
    ],
    responses: {
      200: {
        description: 'Pong',
        content: {
          'application/json': {
            schema: {
              type: 'string',
            }
          }
        }
      },
    }
  }

  return operations
}
