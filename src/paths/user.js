module.exports = user

function user(userService) {
  const operations = {
    POST
  }

  async function POST(req, res, next) {
    const id = await userService.create(req.app.locals.db, req.body.name, req.body.avatar)
    res.json(id)
  }

  POST.apiDoc = {
    summary: 'Create a user',
    operationId: 'createUser',
    tags: [
      'user'
    ],
    security: [
      { keyScheme: [] }
    ],
    requestBody: {
      content: {
        'application/json': {
          schema: {
            type: 'object',
            properties: {
              name: {
                type: 'string',
                minLength: 3,
              },
              avatar: {
                type: 'string',
                format: 'uri',
              },
            },
            required: [
              'name',
              'avatar',
            ],
          },
        },
      },
      required: true,
    },
    responses: {
      200: {
        description: 'User ID',
        content: {
          'application/json': {
            schema: {
              type: 'string',
            }
          }
        }
      },
    }
  }

  return operations
}
