const bodyParser = require('body-parser')
const pino = require('express-pino-logger')
const { initialize } = require('express-openapi')

const app = require('express')()

const utils = require('./utils')
const pack = require('../package.json')

app.disable('x-powered-by')
app.disable('verbose errors')

if (process.env.TESTING !== 'yes') {
  app.use(pino())
}
app.use(bodyParser.json())

const config = Object.assign({}, pack.config)
config.name = pack.name
config.description = pack.description
config.version = pack.version
config.apiKey = process.env.API_KEY || pack.config.apiKey || 'matias'
app.locals.config = config

app.use(utils.cors)

if (process.env.NO_DB !== 'yes') {
  app.locals.dbPromise = utils.database(app)
}

app.locals.api = utils.oasRegister(app, initialize)
app.use(utils.notFound)

module.exports = app
