const { MongoClient } = require('mongodb')
const helloService = require('./services/hello')
const userService = require('./services/user')
const articleService = require('./services/article')

module.exports = { cors, oasRegister, notFound, database, resJson }

function cors(req, res, next) {
  if (req.method === 'OPTIONS') {
    res.statusCode = 200
    res.setHeader('access-control-max-age', '86400') // 86400: 24 hs
  }
  res.setHeader('access-control-allow-origin', '*')
  res.setHeader('access-control-allow-methods', 'GET, POST, DELETE, HEAD, OPTIONS')
  res.setHeader('access-control-allow-headers', 'Origin, X-Requested-With, Content-Type, Accept, Range, Authorization, X-Request-ID')

  if (req.method === 'OPTIONS') {
    return res.end()
  }
  next()
}

function oasRegister(app, initialize) {
  const conf = app.locals.config
  const apiDoc = {
    openapi: '3.0.0',
    servers: [
      {
        url: `${conf.protocol}://${conf.host}:${conf.port}/api/v1`,
      },
    ],
    info: {
      title: conf.name,
      description: conf.description,
      version: conf.version,
    },
    components: {
      securitySchemes: {
        keyScheme: {
          type: 'apiKey',
          name: 'api_key',
          in: 'header'
        }
      },
    },
    paths: {},
  }

  return initialize({
    app,
    apiDoc,
    dependencies: {
      helloService,
      userService,
      articleService,
    },
    securityHandlers: {
      keyScheme
    },
    paths: './src/paths',
    errorMiddleware,
  })

  function keyScheme(req, scopes, definition) {
    const apiKey = req.headers.api_key
    if (apiKey === conf.apiKey) {
      return Promise.resolve(true)
    }
    const error = { statusCode: 403, message: 'API key is not verified' }
    return Promise.reject(error)
  }

  function errorMiddleware(err, req, res, next) {
    let statusCode = 500
    if (err.statusCode || err.status) {
      statusCode = err.statusCode || err.status
    }

    const out = {
      // statusCode,
      message: err.statusMessage || err.message,
    }
    if (err.errors && !err.body) {
      err.body = JSON.stringify(err.errors)
    }
    if (err.body) {
      out.body = err.body
    }

    res.status(statusCode).json(out)

    return next(out.statusMessage, req, res, next)
  }
}

function notFound(req, res, next) {
  if (!res.headersSent) {
    res.status(404).json({ message: 'Not Found' })
  }
}

function database(app) {
  const conf = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
  return new Promise((resolve, reject) => {
    MongoClient.connect(app.locals.config.dbUri, conf, (err, client) => {
      if (err) {
        // eslint-disable-next-line no-console
        console.error(err)
        return reject(process.exit(100))
      }
      app.locals.db = client.db()
      app.locals.appClose = appClose

      process.on('exit', (code) => {
        app.locals.appClose()
      })
      process.on('SIGINT', (code) => {
        process.exit()
      })

      function appClose() {
        client.close()
      }

      return resolve()
    })
  })
}

function resJson(obj) {
  const status = this.statusCode || 200
  this.writeHead(status, { 'Content-Type': 'application/json; charset=utf-8' })
  return this.end(JSON.stringify(obj))
}
