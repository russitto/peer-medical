module.exports = { create }

function create(db, name, avatar) {
  const toInsert = {
    name,
    avatar
  }
  return new Promise((resolve, reject) => {
    db.collection('user').insertOne(toInsert, (err, result) => {
      if (err) {
        return reject(err)
      }
      resolve(result.insertedId)
    })
  })
}
