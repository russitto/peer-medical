module.exports = { create, update, dellete, find }

function create(db, userId, title, text, tags) {
  const toInsert = {
    userId,
    title,
    text,
    tags
  }
  return new Promise((resolve, reject) => {
    db.collection('article').insertOne(toInsert, (err, result) => {
      if (err) {
        return reject(err)
      }
      resolve(result.insertedId)
    })
  })
}

function update(db, articleId, userId, title, text, tags) {
  const toUpdate = {
    userId,
    title,
    text,
    tags
  }
  return new Promise((resolve, reject) => {
    db.collection('article').updateOne({ _id: articleId }, { $set: toUpdate }, (err, result) => {
      if (err) {
        return reject(err)
      }
      resolve(articleId)
    })
  })
}

function dellete(db, articleId) {
  return new Promise((resolve, reject) => {
    db.collection('article').deleteOne({ _id: articleId }, (err, result) => {
      if (err) {
        return reject(err)
      }
      resolve(articleId)
    })
  })
}

function find(db, tag) {
  return new Promise((resolve, reject) => {
    db.collection('article').find({ tags: { $all: [tag] } }, (err, result) => {
      if (err) {
        return reject(err)
      }
      resolve(result.toArray())
    })
  }).then(articles => {
    return articles
  })
}
