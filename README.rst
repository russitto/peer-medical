============
Peer Medical
============

-------------
Configuration
-------------

* Configuration is located at package.json, *config* attribute
* MongoDB default server is localhost

API Token
=========

1. API Token can be configured at env var API_KEY
2. Or at package.json config section as apiKey attribute
3. Otherwhise, default value is *matias*

---
Doc
---

* Please see *static/openapi.json*, and use it with swagger UI (http://editor.swagger.io/)

----
TODO
----

* Isolate test DB
* Test lib/utils.js
* Increase branch coverage
* Validate user exists at article create/update, or use $lookup
* Test validation schemas with ajv package
