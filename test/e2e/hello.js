const tap = require('tap')
const inject = require('light-my-request')
const app = require('../../src/app')
const utils = require('../../src/utils')
const service = require('../../src/services/hello')
const { GET } = require('../../src/paths/ping')(service)

app.locals.dbPromise.then(() => {
  tap.test('GET `/api/v1/ping` route', (t) => {
    t.tearDown(() => app.locals.appClose())

    inject(dispatch, { method: 'get', url: '/api/v1/ping' }, (err, res) => {
      if (err) {
        throw err
      }
      t.assert(res.body, '"pong"')
      t.end()
    })
  })

  function dispatch(req, res) {
    res.json = utils.resJson
    GET(req, res)
  }
})
