const tap = require('tap')
const inject = require('light-my-request')
const app = require('../../src/app')
const utils = require('../../src/utils')
const service = require('../../src/services/user')
const { POST } = require('../../src/paths/user')(service)

app.locals.dbPromise.then(() => {
  tap.test('POST `/api/v1/user` route', (t) => {
    t.tearDown(() => app.locals.appClose())

    const request = {
      method: 'post',
      url: '/api/v1/user',
      headers: {
        'content-type': 'application/json',
      },
      payload: {
        name: 'Ian',
        avatar: 'https://e00-elmundo.uecdn.es/elmundo/imagenes/2009/11/03/1257246349_0.jpg',
      },
    }
    inject(dispatch, request, (err, res) => {
      if (err) {
        throw err
      }
      t.ok(res.body, /^"[a-z0-9]+"$/, 'id is alphanum with quotes')
      t.end()
    })

    function dispatch(req, res) {
      res.json = utils.resJson
      req.app = {
        locals: {
          db: app.locals.db
        }
      }
      req.body = request.payload
      POST(req, res)
    }
  })
})
