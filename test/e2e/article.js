const tap = require('tap')
const inject = require('light-my-request')
const app = require('../../src/app')
const utils = require('../../src/utils')
const service = require('../../src/services/article')
const { POST: postCreate } = require('../../src/paths/article/create')(service)
const { POST: postUpdate } = require('../../src/paths/article/update')(service)
const { POST: postDelete } = require('../../src/paths/article/delete')(service)
const { GET: getFind } = require('../../src/paths/find')(service)

app.locals.dbPromise.then(() => {
  tap.test('CRUD article', (childTest) => {
    childTest.tearDown(() => app.locals.appClose())
    const testCount = 4

    childTest.test('POST `/api/v1/article/create` route', (t) => {
      const request = {
        method: 'post',
        url: '/api/v1/article/create',
        headers: {
          'content-type': 'application/json',
        },
        payload: {
          userId: '5d7653c9fad26122cca52b2d',
          title: 'Facebook podía ocultar el total de "likes" en las publicaciones de usuarios',
          text: 'La red social Facebook contempla una prueba que consiste en dejar de mostrar la cantidad de “likes”…',
          tags: [
            'video',
            'redes',
            'tecno',
          ],
        },
      }
      inject(dispatch, request, (err, res) => {
        if (err) {
          throw err
        }
        t.ok(res.body, /^"[a-z0-9]+"$/, 'id is alphanum with quotes')
        t.end()
        if (childTest.counts.total === testCount) childTest.end()
      })

      function dispatch(req, res) {
        res.json = utils.resJson
        req.app = {
          locals: {
            db: app.locals.db
          }
        }
        req.body = request.payload
        postCreate(req, res)
      }
    })

    childTest.test('POST `/api/v1/article/update` route', (t) => {
      const request = {
        method: 'post',
        url: '/api/v1/article/update',
        headers: {
          'content-type': 'application/json',
        },
        payload: {
          articleId: '5d7f53c9fad26122cca52b2d',
          userId: '5d7653c9fad26122cca52b2d',
          title: 'Facebook podía ocultar el total de "likes" en las publicaciones de usuarios',
          text: 'La red social Facebook contempla una prueba que consiste en dejar de mostrar la cantidad de “likes”…',
          tags: [
            'video',
            'redes',
            'tecno',
          ],
        },
      }
      inject(dispatch, request, (err, res) => {
        if (err) {
          throw err
        }
        t.ok(res.body, /^"[a-z0-9]+"$/, 'id is alphanum with quotes')
        t.end()
        if (childTest.counts.total === testCount) childTest.end()
      })

      function dispatch(req, res) {
        res.json = utils.resJson
        req.app = {
          locals: {
            db: app.locals.db
          }
        }
        req.body = request.payload
        postUpdate(req, res)
      }
    })

    childTest.test('POST `/api/v1/article/delete` route', (t) => {
      const request = {
        method: 'post',
        url: '/api/v1/article/delete',
        headers: {
          'content-type': 'application/json',
        },
        payload: {
          articleId: '5d7653c9fad26122cca52b2d',
        },
      }
      inject(dispatch, request, (err, res) => {
        if (err) {
          throw err
        }
        t.ok(res.body, /^"[a-z0-9]+"$/, 'id is alphanum with quotes')
        t.end()
        if (childTest.counts.total === testCount) childTest.end()
      })

      function dispatch(req, res) {
        res.json = utils.resJson
        req.app = {
          locals: {
            db: app.locals.db
          }
        }
        req.body = request.payload
        postDelete(req, res)
      }
    })

    childTest.test('GET `/api/v1/find` route', (t) => {
      const request = {
        method: 'get',
        url: '/api/v1/find',
        query: {
          tag: 'video'
        }
      }
      inject(dispatch, request, (err, res) => {
        if (err) {
          throw err
        }
        // @TODO check output with ajv package
        t.assert(typeof res.body, 'array')
        t.end()
        if (childTest.counts.total === testCount) childTest.end()
      })

      function dispatch(req, res) {
        res.json = utils.resJson
        req.app = {
          locals: {
            db: app.locals.db
          }
        }
        req.query = request.query
        getFind(req, res)
      }
    })
  })
})
