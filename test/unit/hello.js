const tap = require('tap')
const app = require('../../src/app')
const service = require('../../src/services/hello')

app.locals.dbPromise.then(() => {
  tap.test('unit: hello service', (t) => {
    t.tearDown(() => app.locals.appClose())
    tap.test('--> hello', (t) => {
      const out = service.ping()
      t.assert(out, 'pong')
      t.end()
    })
    t.end()
  })
})
