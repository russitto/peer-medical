const tap = require('tap')
const app = require('../../src/app')
const service = require('../../src/services/user')

app.locals.dbPromise.then(() => {
  tap.test('unit: user service', (t) => {
    t.tearDown(() => app.locals.appClose())

    const img = 'https://i.ytimg.com/vi/tIBI1V21YCM/maxresdefault.jpg'
    service.create(app.locals.db, 'Juancito', img).then((out) => {
      t.assert(out, /[a-z0-9]+/, 'alphanum')
      t.end()
    })
  })
})
