const tap = require('tap')
const app = require('../../src/app')
const service = require('../../src/services/article')

app.locals.dbPromise.then(() => {
  tap.test('unit: article service', async (t) => {
    t.tearDown(() => app.locals.appClose())

    const userId = '5d7653c9fad26122cca52b2d'
    const title = 'Facebook podía ocultar el total de "likes" en las publicaciones de usuarios'
    const text = 'La red social Facebook contempla una prueba que consiste en dejar de mostrar la cantidad de “likes”…'
    const tags = [
      'sandia',
      'con',
      'vino',
      'video',
    ]
    const articleId = await service.create(app.locals.db, userId, title, text, tags)
    t.assert(articleId, /[a-z0-9]+/, 'alphanum')

    const idUpd = await service.update(app.locals.db, articleId, userId, title, text, tags)
    t.assert(articleId, /[a-z0-9]+/, 'alphanum')
    t.assert(articleId, idUpd)

    const idDel = await service.dellete(app.locals.db, articleId)
    t.assert(articleId, /[a-z0-9]+/, 'alphanum')
    t.assert(articleId, idDel)

    const articles = await service.find(app.locals.db, 'video')
    t.assert(typeof articles, 'array')

    t.end()
  })
})
